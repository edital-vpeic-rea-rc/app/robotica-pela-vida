
/*
  robozinho1
  testa a placa do robozinho com leds, buzzer e servo motor.
  A placa robozinho utiliza dois dels azuis nos olhos, um led azul na boca,
  três leds para sinal de trânsito (verde, amarelo e vermelho), dois leds 
  par a sinal de pedestre (verde e vermelho), um buzzer e um servo motor.
  13 - led olho esquerdo          8 - led verde pedestre
  12 - led olho direito           7 - led vermelho pedestre
  11 - led verde                  6 - servo motor
  10 - led amarelo                5 - buzzer
   9 - led vermelho               4 - led boca
  Este código exemplo foi desenvolvido para o projeto LabVad.
  wwww.labvad.com 
  Atualizado em junho de 2017
  por César Bastos

*/

// inclui a biblioteca de servo motores
#include <Servo.h>

Servo myservo;        // cria um objeto servo para controlar o servo motor
                      // até 12 objetos do tipo servo podem ser criados na maioria das placas

int pos = 0;          // cria a variável pos com valor 0 para guardar a posição inicial do servo motor
int pino_buzzer = 5;  // cria a variável pino_buzzer para uso na pino 5

// A função setup é executada apenas uma vez quando você reinicialisa (reset) ou liga a placa 
void setup() {
  Serial.begin(9600);     // configura a Baund Rate de comunicação para 9600
                        
  pinMode(3, OUTPUT);     // iniciando os pinos digitais de 3 a 13 como pinos de saída (output).                   
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);  
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);  
  myservo.attach(6);              // configura o pino 6 PWM para o objeto servo 
  pinMode(pino_buzzer, OUTPUT);   // configura o pino 5 PWM para o Buzzer
}

// A função loop é executada repetidamente   
void loop() {

// GIRANDO A PÁ DA MÃO DIREITA 
   Serial.println("Testando a mão!");  // escreve na janela do Monitor Serial
 for (pos = 50; pos <= 120; pos += 1) { // vai de 50 graus até 120 graus 
                                        // em passos de 1 grau
    myservo.write(pos);              // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                       // espera 15ms (ou 0,15seg) 
  }
  for (pos = 120; pos >= 50; pos -= 1) {  // vai de 120 graus até 50 graus 
    myservo.write(pos);                   // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                            // espera 15ms (ou 0,15seg) 
  }
  for (pos = 30; pos <= 150; pos += 1) {  // vai de 30 graus até 150 graus 
                                          // em passos de 1 grau
    myservo.write(pos);                   // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                            // espera 15ms (ou 0,15seg) 
  }
  for (pos = 150; pos >= 30; pos -= 1) {  // vai de 150 graus até 30 graus 
    myservo.write(pos);                   // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                            // espera 15ms (ou 0,15seg) 
  }
for (pos = 50; pos <= 120; pos += 1) {  // vai de 50 graus até 120 graus 
                                        // em passos de 1 grau
    myservo.write(pos);                 // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                          // espera 15ms (ou 0,15seg) 
  }
  for (pos = 120; pos >= 50; pos -= 1) {  // vai de 120 graus até 50 graus 
    myservo.write(pos);                   // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                            // espera 15ms (ou 0,15seg) 
  }
for (pos = 100; pos <= 150; pos += 1) {   // vai de 100 graus até 150 graus 
    // em passos de 1 grau
    myservo.write(pos);                   // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                            // espera 15ms (ou 0,15seg) 
  }
  for (pos = 150; pos >= 100; pos -= 1) { // vai de 150 graus até 100 graus 
    myservo.write(pos);                   // diz para o servo seguir para a posição da variável 'pos'
    delay(50);                            // espera 15ms (ou 0,15seg) 
  }
  // Serial.println(">>>>>>>>>>   Começando novo cliclo   <<<<<<<<<<<!");  // escreve na janela do Monitor Serial
}
