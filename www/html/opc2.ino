
/*
  robozinho1
  testa a placa do robozinho com leds, buzzer e servo motor.
  A placa robozinho utiliza dois dels azuis nos olhos, um led azul na boca,
  três leds para sinal de trânsito (verde, amarelo e vermelho), dois leds 
  par a sinal de pedestre (verde e vermelho), um buzzer e um servo motor.
  13 - led olho esquerdo          8 - led verde pedestre
  12 - led olho direito           7 - led vermelho pedestre
  11 - led verde                  6 - servo motor
  10 - led amarelo                5 - buzzer
   9 - led vermelho               4 - led boca
  Este código exemplo foi desenvolvido para o projeto LabVad.
  wwww.labvad.com 
  Atualizado em junho de 2017
  por César Bastos

*/

// inclui a biblioteca de servo motores
#include <Servo.h>

Servo myservo;        // cria um objeto servo para controlar o servo motor
                      // até 12 objetos do tipo servo podem ser criados na maioria das placas

int pos = 0;          // cria a variável pos com valor 0 para guardar a posição inicial do servo motor
int pino_buzzer = 5;  // cria a variável pino_buzzer para uso na pino 5

// A função setup é executada apenas uma vez quando você reinicialisa (reset) ou liga a placa 
void setup() {
  Serial.begin(9600);     // configura a Baund Rate de comunicação para 9600
                        
  pinMode(3, OUTPUT);     // iniciando os pinos digitais de 3 a 13 como pinos de saída (output).                   
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);  
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);  
  myservo.attach(6);              // configura o pino 6 PWM para o objeto servo 
  pinMode(pino_buzzer, OUTPUT);   // configura o pino 5 PWM para o Buzzer
}

// A função loop é executada repetidamente   
void loop() {
  // PISCANDO OLHOS E BOCA
  Serial.println("Testando os olhos!");   // escreve na janela do Monitor Serial
  digitalWrite(13, HIGH);                 // liga o olho esquerdo 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, LOW);                  // desliga o olho esquerdo
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, HIGH);                 // liga o olho direito 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, LOW);                  // desliga o olho direito 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, HIGH);                 // liga o olho esquerdo 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, LOW);                  // desliga o olho esquerdo
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, HIGH);                 // liga o olho direito
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, LOW);                  // desliga o olho direito
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, HIGH);                 // liga o olho esquerdo
  digitalWrite(12, HIGH);                 // liga o olho direito
  delay(500);                             // espera 0,5 segundo
  digitalWrite(13, LOW);                  // turn the LED off by making the voltage LOW
  digitalWrite(12, LOW);                  // turn the LED off by making the voltage LOW
  delay(500);                             // espera 0,5 segundo
  digitalWrite(4, HIGH);                  // liga a boca 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, LOW);                   // desliga a boca
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, HIGH);                  // liga a boca 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, LOW);                   // desliga a boca
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, HIGH);                  // liga a boca 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, LOW);                   // desliga a boca
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, HIGH);                 // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(500);                             // espera 0,5 segundo
  digitalWrite(13, LOW);                  // turn the LED off by making the voltage LOW
  digitalWrite(12, LOW);                  // turn the LED off by making the voltage LOW
  delay(500);                             // espera 0,5 segundo
  digitalWrite(13, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, LOW);                  // turn the LED off by making the voltage LOW
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, LOW);                  // turn the LED off by making the voltage LOW
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, LOW);                  // turn the LED off by making the voltage LOW
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(300);                             // espera 0,3 segundo
  digitalWrite(12, LOW);                  // turn the LED off by making the voltage LOW
  delay(300);                             // espera 0,3 segundo
  digitalWrite(13, HIGH);                 // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(500);                             // espera 0,5 segundo
  digitalWrite(13, LOW);                  // turn the LED off by making the voltage LOW
  digitalWrite(12, LOW);                  // turn the LED off by making the voltage LOW
  delay(500);                             // espera 0,5 segundo
  digitalWrite(13, HIGH);                 // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, HIGH);                 // turn the LED on (HIGH is the voltage level)
  delay(500);                             // espera 0,5 segundo
  digitalWrite(13, LOW);                  // turn the LED off by making the voltage LOW
  digitalWrite(12, LOW);                  // turn the LED off by making the voltage LOW
  delay(500);                             // espera 0,5 segundo
  digitalWrite(4, HIGH);                  // liga a boca 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, LOW);                   // desliga a boca
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, HIGH);                  // liga a boca 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, LOW);                   // desliga a boca
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, HIGH);                  // liga a boca 
  delay(300);                             // espera 0,3 segundo
  digitalWrite(4, LOW);                   // desliga a boca
  delay(300);                             // espera 0,3 segundo

  // Serial.println(">>>>>>>>>>   Começando novo cliclo   <<<<<<<<<<<!");  // escreve na janela do Monitor Serial
}
