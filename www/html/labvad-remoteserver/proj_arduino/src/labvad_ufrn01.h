// ------------------------------------------------------------------------
// Arquivo labvad_uno.h 
// Definição das portas utilizadas pela placa do laboratório <LabVAD-UFRN01>
// com arduino uno.
//
// Maurício Bomfim - 09/2015
// ------------------------------------------------------------------------

// LEDs
// Uso:
// pinMode(LED_VM1, OUTPUT);    // Inicializa primeiro LED vermelho
// digitalWrite(LED_VM1, HIGH); // Acende o primeiro LED vermelho
// digitalWrite(LED_VM1, LOW);  // Apaga o primeiro LED vermelho

#define LED_VM1  9  // primeiro LED vermelho
#define LED_AM1  8  // primeiro LED amarelo
#define LED_VD1  7  // primeiro LED verde
#define LED_AZ1  6  // primeiro LED azul

#define LED_VM2 10  // segundo LED vermelho
#define LED_AM2 11  // segundo LED amarelo
#define LED_VD2 12  // segundo LED verde
#define LED_AZ2 13  // segundo LED azul


// Display de caracteres
// Uso:
// #include <LiquidCrystal.h>
// LiquidCrystal lcd(RS, RW, EN, D4, D5, D6, D7);

#define RS  6  //
#define RW  7  // read/write
#define EN  8  // enable
#define D4  9  // dado 4
#define D5 10  // dado 5
#define D6 11  // dado 6
#define D7 12  // dado 7


// Display de 7 segmentos
// Uso:
// pinMode(SEG_A,OUTPUT); // Inicializa segmento A
// digitalWrite(SEG_A,HIGH); // Acende segmento A
// digitalWrite(SEG_A,LOW);  // Apaga segmento A

#define SEG_A  6  // Segmento A
#define SEG_B  7  // Segmento B
#define SEG_C  8  // Segmento C
#define SEG_D  9  // Segmento D
#define SEG_E 10  // Segmento E
#define SEG_F 11  // Segmento F
#define SEG_G 12  // Segmento G
#define PD    13  // Ponto Decimal


// LED RGB
// Uso:
// Inicializa as 3 componentes RGB
//   pinMode(RGB_VM, OUTPUT); // R
//   pinMode(RGB_VD, OUTPUT); // G
//   pinMode(RGB_AZ, OUTPUT); // B
// Acende vermelho
//   analogWrite(RGB_VM, 255); // R
//   analogWrite(RGB_AZ, 0);   // G
//   analogWrite(RGB_VD, 0);   // B

#define RGB_VM 10  // Componente R
#define RGB_VD 12  // Componente G
#define RGB_AZ 11  // Componente B


// Buzzer

#define BUZZER 6


// Servo 
// Uso:
// meu_servo.attach(SERVO); // Inicializa servo motor
// meu_servo.write(45);     // Posiciona servo a 45 graus da origem

#define SERVO 9


// Relé
// Uso:
// pinMode(RELE, OUTPUT);    // Inicializa o Relé
// digitalWrite(RELE, HIGH); // Aciona o Relé
// digitalWrite(RELE, LOW);  // Desliga o Relé

#define RELE 7 


// Motor DC
// Uso:
// pinMode(MOTOR_DC, OUTPUT);   // Inicializa o Motor
// analogWrite(MOTOR_DC, 125);  // Aciona o Motor na velocidade máxima ( 0 <= valor <= 125 )
// analogWrite(MOTOR_DC, 0);    // Desliga o motor

#define MOTOR_DC 8


