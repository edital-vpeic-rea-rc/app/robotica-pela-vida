// ------------------------------------------------------------------------
// Arquivo labvad_mega2560.h 
// Definição das portas utilizadas pela placa LabVAD-UFRJ01
// com arduino mega2560.
//
// Maurício Bomfim - 09/2015
// ------------------------------------------------------------------------

// LEDs
// Uso:
// pinMode(LED_VM1, OUTPUT);    // Inicializa primeiro LED vermelho
// digitalWrite(LED_VM1, HIGH); // Acende o primeiro LED vermelho
// digitalWrite(LED_VM1, LOW);  // Apaga o primeiro LED vermelho

 #define LED_VM1 39 // primeiro LED vermelho
 #define LED_AM1 41 // primeiro LED amarelo
 #define LED_VD1 43 // primeiro LED verde
 #define LED_AZ1 45 // primeiro LED azul

 #define LED_VM2 44 // segundo LED vermelho
 #define LED_AM2 42 // segundo LED amarelo
 #define LED_VD2 40 // segundo LED verde
 #define LED_AZ2 38 // segundo LED azul

// Display de caracteres
// Uso:
// #include <LiquidCrystal.h>
// LiquidCrystal lcd(RS, RW, EN, D4, D5, D6, D7);

 #define RS 31   //
 #define RW 33   // read/write
 #define EN 35   // enable
 #define D4 30   // dado 4
 #define D5 32   // dado 5
 #define D6 34   // dado 6
 #define D7 36   // dado 7

// Display de 7 segmentos
// Uso:
// pinMode(SEG_A,OUTPUT); // Inicializa segmento A
// digitalWrite(SEG_A,HIGH); // Acende segmento A
// digitalWrite(SEG_A,LOW);  // Apaga segmento A

 #define SEG_A 23  // Segmento A
 #define SEG_B 25  // Segmento B
 #define SEG_C 29  // Segmento C
 #define SEG_D 28  // Segmento D
 #define SEG_E 26  // Segmento E
 #define SEG_F 22  // Segmento F
 #define SEG_G 24  // Segmento G
 #define PD 27     // Ponto Decimal

// LED RGB
// Uso:
// Inicializa as 3 componentes RGB
//   pinMode(RGB_VM, OUTPUT); // R
//   pinMode(RGB_VD, OUTPUT); // G
//   pinMode(RGB_AZ, OUTPUT); // B
// Acende vermelho
//   analogWrite(RGB_VM, 255); // R
//   analogWrite(RGB_AZ, 0);   // G
//   analogWrite(RGB_VD, 0);   // B

 #define RGB_VM 13  // Componente R
 #define RGB_VD 12  // Componente G
 #define RGB_AZ 11  // Componente B

// Servo Motor
// Uso:
// meu_servo.attach(SERVO); // Inicializa servo motor
// meu_servo.write(45);     // Posiciona servo a 45 graus da origem

 #define SERVO 37

// Relé
// Uso:
// pinMode(RELE, OUTPUT); // Inicializa o Relé
// digitalWrite(RELE, HIGH); // Aciona o Relé
// digitalWrite(RELE, LOW);  // Desliga o Relé

 #define RELE 49 

// Motor DC
// Uso:
// pinMode(MOTOR_DC, OUTPUT); // Inicializa o Motor
// analogWrite(MOTOR_DC, 125);  // Aciona o Motor na velocidade máxima ( 0 <= valor <= 125 )
// analogWrite(MOTOR_DC, 0);    // Desliga o motor

 #define MOTOR_DC 6 


