<?php error_reporting(E_ALL);
	Header('Vary: User-Agent');
	function arduinoBuildUpload($projeto) {
	global $debug, $verbose;

	$error = 0;
//	echo $_SERVER['DOCUMENT_ROOT']."/$projeto";
    if (chdir($_SERVER['DOCUMENT_ROOT']."/$projeto")){
        if ($verbose=='T') $debug .= "chdir ok: ".getcwd()."\n";
	// echo "PASSOU 6";
        // executa compilação jogando a saida de erro padrão na saída padrão
        // para que os erros de compilação retornem em $output
//        exec("ino clean", $output, $return_status);
//        exec("sudo rm -R .build/");
//	exec("ino clean", $output, $return_status);
//        exec("ino build 2>&1", $output, $return_status);
//        exec("ino build 2>&1");
//	exec("ino build 2>&1", $output, $return_status);
	exec("ino build", $output, $return_status);
	exec("ino upload", $output, $return_status);

//	echo "PASSOU 7";
//	echo $return_status;

        if ($verbose=='T') $debug .= 'ino build: '.$return_status."\n";
        if ($verbose=='T') $debug .= print_r($output, TRUE);
        if ($verbose=='T') $debug .= "\n";
	//echo "PASSOU 8";
        // se não deu erro de compilação...
        if ($return_status==0){
	//echo "PASSOU 9";
            // transfere o programa compilado para executar na placa
            exec('ino upload', $output, $return_status);
            if ($return_status!=0) {
                $error = 4; // erro de execução
            }
            if ($verbose=='T') $debug .= 'ino upload: '.$return_status."\n";
            if ($verbose=='T') $debug .= print_r($output, TRUE);
            if ($verbose=='T') $debug .= "\n";
        }
        else {
            $error = 3; // erro de compilação
        }
    }
    else {
        $error = 2; // erro ao executar chdir
        if ($verbose=='T') $debug .= "chdir error: ".getcwd()."\n";
    }
    return array($output, $error); // array(array(), 0)
}
$debug = '';
$verbose = isset($_POST['verbose']) ? $_POST['verbose'] : 'F';
if (isset($_GET['codigo'])) {
    if ($_GET['codigo']=='zerar') {
		// compila e executa código para zerar a placa
        $r = arduinoBuildUpload('proj_zerar');
	    $rJson = array(
		"debug" => $debug,
		"msg" => ($r[1]==0)?"Zerou com sucesso":join("\n",$r[0]),
		"erro" => $r[1]);
    }
}
if (isset($_GET['codigo'])) {
	// echo $_GET['codigo'];
	$cod = $_GET['codigo'];
	// echo 'Codigo ' .$cod;
	$arq = '../opc' .$cod. '.ino';
	// echo $_POST['codigo'];
//	if (($_GET['codigo']==1) || ($_GET['codigo']==2) ||($_GET['codigo']==3) || ($_GET['codigo']=='4') || ($_GET['codigo']=='5')) {
	if ($cod > 0) {

//		echo 'PASSOU 1';
//		echo $arq;
//
		$arquivo = fopen($arq, 'r+');
		while(!feof($arquivo)) {
			$linha = fgets($arquivo, 4096);
			$cod2 = $cod2 . $linha;
			$codigoMontado = $cod2;
		}
		//echo $cod2;
		//$codigoMontado = $cod2;
		// echo $linha;

//		echo "Codigo montado ";
//		echo $codigoMontado;

//		fclose($arquivo);

//		$arq = fopen('/proj_arduino/src/dados.ino', 'w');
//		fwrite($arq, $codigoMontado);

//		echo $arq;

		fclose($arquivo);

if (copy($arq, 'proj_arduino/src/dados.ino')){
//echo "Arquivo copiado com Sucesso.";
}
else
{
echo "Erro ao copiar arquivo.";
}
		$r = arduinoBuildUpload('proj_arduino');
		$rJson = array(
		"debug" => $debug,
		"msg" => ($r[1]==0)?"Execução com sucesso":join("\n",$r[0]),
		"erro" => $r[1]);
	}
	else {
        // grava num arquivo .ino o código recebido por POST
	    $codigoMontado = urldecode($_POST['codigo']);
//	    $codigoMontado = $_POST['codigo'];
//		echo $_POST['codigo'];
		$arq = fopen('temp.ino', 'w+');
		fwrite($arq, $codigoMontado);
		fclose($arq);

		$arq='temp.ino';
if (copy($arq, 'proj_arduino/src/dados.ino')){
//echo "Arquivo copiado com Sucesso. 2";
}
else
{
echo "Erro ao copiar arquivo. 2";
}
		$r = arduinoBuildUpload('proj_arduino');
		$rJson = array(
		"debug" => $debug,
		"msg" => ($r[1]==0)?"Execução com sucesso":join("\n",$r[0]),
		"erro" => $r[1]);
	}
}
else {
	$rJson = array(
	"debug" => $debug,
	"msg" => 'Código não encontrado!',
	"erro" => 1);
}
$anterior = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $fallback;
header("location: {$anterior}");
exit;
?>
