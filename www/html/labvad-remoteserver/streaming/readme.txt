LabVAD-RemoteServer-Streaming_0.1.21-beta

Instruções gerais:
   * Na Pasta streaming *
   O arquivo 'streaming' é usado para começar um stream de vídeo. É passado por parâmetro, no terminal, ou a palavra 'start' ou a palavra 'stop'.
'start' inicia o stream e 'stop' o interrompe.
   O arquivo 'streaming.conf' é um arquivo de configuração sobre o stream de vídeo nas páginas php.
   * Na pasta php *
   O arquivo 'playStream.php' dispõe o stream em um player de vídeo.
   O arquivo 'recordStream.php' salva o stream (veja o 'vlc.ini' para diretório, extensão e mais).
   O arquivo 'stopStreamRecording.php' interrompe a gravação do stream, caso exista uma.
   O arquivo 'showOffPage.php' é a página teste de demonstração das funções. Note que precisa-se ter um stream iniciado para poder visualizá-la da maneira que se deve.
   Os arquivos 'ajax_parar.txt' e 'ajax_gravar.txt' fazem parte do feedback de gravação da página 'showOffPage.php'.
   O arquivo 'videoList.php' é a página que lista os vídeos do diretório especificado no arquivo 'vlc.ini'.
   O arquivo 'savedVideoPlayer.php' é a página que recebe, via POST, o arquivo especificado na página 'videoList.php'.
   O arquivo 'startVideoRec.php' é a página que implementa o método de gravação de um certo período

Log das versões:

   LabVAD-Server-VLC_0.1.1-beta:
        Primeira versão.

   LabVAD-Server-VLC_0.1.2-beta:
        - Correção do bug que ocorria no arquivo 'vlc.sh';
        - Adição da página de demonstração;
        - Adição de pequenos detalhes no arquivo 'vlc.ini'.

   LabVAD-Server-VLC_0.1.3-beta:
	    - Correção do acesso a webcam.

   LabVAD-Server-VLC_0.1.4-beta:
        - Adição da listagem e visualização dos vídeos salvos;
	    - Algumas mudanças na página de teste, 'showOffPage.php'.

   LabVAD-RemoteServer-Streaming_0.1.5-beta:
        - Algumas definições usadas no 'vlc.sh' em 'vlc.ini';
        - Migração do visualizador de vídeos: Do plugin dependente do VLC para o HTML5;
        - Várias opções adicionadas em 'vlc.ini', incluindo o acesso à câmera USB;
        - Correção de um pequeno problema em 'videoList.php';
        - Migração da stream de mpg para ogg.

   LabVAD_RemoteServer-Streaming_0.1.6-beta:
        - Redefinição da estrutura de pastas;
        - Adição da funcionalidade de log;
        - Adição da legenda do vídeo;
        - Definição de outras propriedades no arquivo streaming.conf

   LabVAD-RemoteServer-Streaming_0.1.7-beta:
        - Introdução a cópia remota de arquivos;
        - Adição de múltiplas legendas no vídeo;
        - Criação da função de gravar vídeo de certa duração;
        - Modificações na estrutura de salvamento de logs.

   LabVAD-RemoteServer-Streaming_0.1.8-beta:
        - Aprimoramento da posição das legendas;
        - Introdução do log da cópia de arquivos;
        - Modificações na estrutura geral das legendas;
        - Modularizando o local do arquivo temporário.

   LabVAD-RemoteServer-Streaming_0.1.9-beta:
        - Mudanças na organização padrão das legendas;
        - Adição do parâmetro de framerate do vídeo no arquivo de coniguração;
        - Adição dos parâmetros do tamanho e largura do vídeo no stream;
        - Optimização dos valores padrões, no arquivo de configuração, para stream mais rápida e leve;
        - Modularização de todos os parâmetros em relação ao codec e a qualidade do vídeo.

   LabVAD-RemoteServer-Streaming_0.2.0-beta:
        - Aplicação do redimensionamento de tamanho da stream em proporção;
        - Solução do bug da legenda;
        - Utilização de um id pré-definido no salvamento de stream;
        - Feedback via JSON do resultado da operação.

   LabVAD-RemoteServer-Streaming_0.1.11-beta:
        - Aprimoramento da velocidade de disposição da stream;
        - Adição de feedbacks adicionais via JSON;
        - Tratamento do caso "root user" na execução do streaming executável;
        - Aprimoramento da estrutura de cópia de arquivos.

   LabVAD-RemoteServer-Streaming_0.1.12-beta:
        - Corrigindo a estrutura de cópia de arquivos;
        - Mudando as permissões de escrita para a pasta de logs;
        - Mudando e corrigindo alguns aspectos no arquivo de configuração.

   LabVAD-RemoteServer-Streaming_0.1.13-beta:
        - Aprimorando e resolvendo a cópia de arquivos;
        - Aprimorando o arquivo de configuração.

   LabVAD-RemoteServer-Streaming_0.1.14-beta:
        - Correção da geração de logs;
        - Adicinando, no arquivo de configuração, opção para vídeo com audio ou não;
        - Adicionando funcionalidade de tempo máximo de gravação;
        - Adicionando outra label para identificar a câmera usada.

   LabVAD-RemoteServer-Streaming_0.1.15-beta:
        - Adição do nome da câmera no arquivo de vídeo.

   LabVAD-RemoteServer-Streaming_0.1.16-beta:
        - Adição da porta de scp;
        - Correção de um pequeno bug no arquivo de configuração.

   LabVAD-RemoteServer-Streaming_0.1.17-beta:
        - Correção do sistema de permissões na pasta de logs.

   LabVad-RemoteServer-Streaming_0.1.18-beta:
        - Modificação do arquivo de configuração;
	- Correção do sistema de cópia de arquivo.

   LabVad-RemoteServer-Streaming_0.1.19-beta:
   	- Retirada do campo de fps do arquivo de configuração, para optimizar a velocidade de streaming;
   	- Modificação de parâmetros de disposição de streaming;
   	- Modificação no retorno de JSON da página;
   	- Adição de flag AV para streams dispostas em audio e vídeo, e V para streams dispostas em apenas vídeo.

   LabVad-RemoteServer-Streaming_0.1.20-beta:
	- Modificação do nome da câmera em disposição no streaming;
	- Correção da permissão de escrita nos logs;
	- Modificação no arquivo de configuração para um stream de maior qualidade;
	- Modificações no sistema de php para gravar e copiar um vídeo.

   LabVad-RemoteServer-Streaming_0.1.21-beta:
	- Adição de logo do laboratório já modularizado no arquivo de configuração;
	- Modificação de parâmetros (dimensões): De escala de vídeo para altura e largura;
	- Modificação da estrutura no arquivo de configuração;
	- Adição de flags para dispor o laboratório com ou sem logo ou background negro.
