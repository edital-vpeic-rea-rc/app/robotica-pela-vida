<!--
 
Parametros da tag embed:
{
   type:        O plugin a ser utilizado na pagina web;
   pluginspage: O website do plugin;
   version:     Versão a ser utilizada do plugin;
   width:       Propriedade das dimensões do player de vídeo do VLC. O campo especifica a altura;
   height:      Propriedade das dimensões do player de vídeo do VLC. O campo especifica a largura;
   id:          Identificador do player de vídeo para uso na página HTML. Como o Stream não é estático, não há necessidade para tal;
   controls:    Campo que especifica se o menu de toolbar do player (play, pause, stop...) fica amostra ou não (true/false. True por padrão);
   target:      O endereço do vídeo a ser disposto no player.
}

-->
<html>
<head>
   <title>LabVAD Server Video Module: Player Example</title>
</head>
<body>
	<?php
	$player_data = parse_ini_file("../streaming.conf");
	echo "<video width=\"".$player_data[player_width]."\" height=\"".$player_data[player_height]."\" autoplay>";
    echo "<source src=\"http://localhost:".$player_data[server_port]."/".$player_data[stream_file]."\" type=\"video/ogg\">";
    echo "</video>";
   ?>
</body>
</html>
