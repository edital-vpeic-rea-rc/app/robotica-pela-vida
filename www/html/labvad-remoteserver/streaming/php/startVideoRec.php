<?php
// Starting the session to get important values
session_start();

// Setting recording file name, directory and extension
$record_data = parse_ini_file("../streaming.conf");

//startVideoRec( "meuStream", 10, $record_data );
abortVideoRec( $record_data );

// Ending session
session_write_close();

function startVideoRec( $nameOfFile, $duration, $record_data ){
	// Verifying extension to use certain transcoding codec
	if($record_data[video_extension] == "mp4"){
		$transcoding = ":sout='#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100,width=".$record_data[video_width].",height=".$record_data[video_height]."}";
		echo "The selected recording extension is mp4!";
	} else if ($record_data[video_extension] == "webm"){
		$transcoding = ":sout='#transcode{vcodec=VP80,vb=2000,acodec=vorb,ab=128,channels=2,samplerate=44100,width=".$record_data[video_width].",height=".$record_data[video_height]."}";
		echo "The selected recording extension is webm!";
	} else if ($record_data[video_extension] == "ogg"){
		$transcoding = ":sout='#transcode{vcodec=theo,vb=800,acodec=vorb,ab=128,channels=2,samplerate=44100,width=800,height=600}";
		echo "The selected recording extension is ogg!";
	}

	// Getting the command
	$cmd = "cvlc --run-time=$duration http://localhost:" . $record_data[server_port] ."/" . $record_data[stream_file] . " " . $transcoding . ":std{access=file{overwrite},mux=".$record_data[video_extension].",dst='" . $record_data[video_directory] . $nameOfFile . ".". $record_data[video_extension] ."'}' vlc://quit";

	//echo "\n\n".$cmd."\n\n";

	// Executing the command
	exec($cmd . " > /dev/null 2>&1 & echo $!", $out, $return);

	if(!$return){
		// If the log flag is true, get the log of the start of the recording
		if($record_data[log] == "true"){
			$r = array("erro" => 1,
      		"msg" => "Unable to write to log...");
			$logFile = fopen("../logs/streaming_".date("Y").date("m").date("d").".log", "a") or die(json_encode($r));
			fwrite($logFile, date("H:i:s")." Recording stream $nameOfFile of extension $record_data[video_extension] in $duration seconds.\n");
			fclose($logFile);
		}

		//echo "\n\nThis is the pid: ". (int)$out[0];

		// Get the PID, the name of the file and the extension of the recording, to use to stop
		$_SESSION["sVRpid"] = (int)$out[0];
		$_SESSION["sVRnameof"] = $nameOfFile.".".$record_data[video_extension];

		$r = array("erro" => 0,
      	"msg" => "Gravação do vídeo iniciada com sucesso.");
      	echo json_encode($r);
	} else {
		$r = array("erro" => 1,
      	"msg" => "A gravação do vídeo falhou.");
      	echo json_encode($r);
	}
}

function abortVideoRec( $record_data ){
	exec( "kill ".$_SESSION["sVRpid"] );
	exec( "rm ".$record_data[html5_video_dir].$_SESSION["sVRnameof"]." " );
	unset($_SESSION["sVRpid"]);
	unset($_SESSION["sVRnameof"]);
}
?>
