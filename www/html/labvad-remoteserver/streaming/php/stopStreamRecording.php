<?php

$stop_data = parse_ini_file("../streaming.conf");

//$labName = explode('.', $stop_data[stream_file]);

$centralVidPath = $stop_data[central_server_video_directory];

//$centralVidPath = $stop_data[central_server_video_directory].$labName[0]."/";

if($stop_data[log] == "true"){
  $r = array("erro" => 1,
"msg" => "Unable to write to log...");
  $logFile = fopen("../logs/streaming_".date("Y").date("m").date("d").".log", "a") or die(json_encode($r));
}

if($_POST["pid"] == NULL){
 $r = array("erro" => 1,
  "msg" => "A gravação do vídeo falhou. Reason: PID is null.");
  echo json_encode($r);

} else {
 //exec("kill " . $_SESSION["pid"], $out, $return);
    exec("sh ".$stop_data[stop_rec_path]." " . $_POST["pid"] . "  2>&1 1> /dev/null", $out, $return);
 // http://servidor/download_video.php
 if(!$return){
       //exec("scp ". $stop_data["video_directory"] . $_SESSION["nameof"] .".".$stop_data["video_extension"]." ".$stop_data["machine_username"]."@".$stop_data["destination_host"].":".$stop_data["destination_folder"]);
   if($stop_data[log] == "true"){
    fwrite($logFile, date("H:i:s")." Stopped the recording of stream ".$stop_data[temp_vid_dir].$_POST["rnameof"].".\n");
  }

  if($stop_data[log] == "true"){
    fwrite($logFile, date("H:i:s")." Started the copy the copy of the file ".$stop_data[temp_vid_dir].$_POST["rnameof"]." to ". $centralVidPath.$_POST["rnameof"] .".\n");
  }

  //exec("scp -o \"StrictHostKeyChecking no\" -P ".$stop_data[copy_port]." ".$stop_data[temp_vid_dir].$_SESSION["rnameof"]." ".$stop_data[central_server_username]."@".$stop_data[central_server_host].":".$centralVidPath.$_SESSION["rnameof"], $out2, $return2);
  //exec("sh /home/murilao/Documents/Webserver/scripts/scpCpy.sh ". $stop_data[copy_port] ." ". $stop_data[temp_vid_dir] ." ". $_POST["rnameof"] ." ". $stop_data[central_server_username] ." ". $stop_data[central_server_host]  ." ". $centralVidPath . "  2>&1 1> /dev/null", $out2, $return2);
  exec("sh ".$stop_data[copy_vid_path]." ". $stop_data[copy_port] ." ". $stop_data[temp_vid_dir] ." ". $_POST["rnameof"] ." ". $stop_data[central_server_username] ." ". $stop_data[central_server_host]  ." ". $centralVidPath . "  2>&1 1> /dev/null", $out2, $return2);
  //exec("scp ".$stop_data[temp_vid_dir].$_SESSION["rnameof"]." ".$_SESSION["nameof"], $out2, $return2);

  if(!$return2){
    if($stop_data[log] == "true"){
      fwrite($logFile, date("H:i:s")." Copy finished.\n");
    }
    $r = array("erro" => 0,
      "msg" => "Video salvo com sucesso.");
      echo json_encode($r);
  } else {
    $dbg_export = var_export($out2, true);
    if($stop_data[log] == "true"){
      fwrite($logFile, date("H:i:s")." Error during saving stream. Reason: \n". $dbg_export ."\n");
    }
    $r = array("erro" => 1,
      "msg" => "A gravação do vídeo falhou. Reason: \n". $dbg_export ."\n");
    echo json_encode($r);
  }
} else {
  if($stop_data[log] == "true"){
    $debug_export = var_export($out, true);
    fwrite($logFile, date("H:i:s")." Error during saving stream. Reason: \n". $debug_export ."\n");
    }
  $r = array("erro" => 1,
    "msg" => "A gravação do vídeo falhou. Aquie!");
  echo json_encode($r);
}
fclose($logFile);
}
?>
