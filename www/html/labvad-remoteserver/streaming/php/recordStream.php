<?php
   header('Content-Type: application/json');
   // Setting recording file name, directory and extension
   $record_data = parse_ini_file("../streaming.conf");
   //$rFileName = uniqid($record_data[video_name] . "_");
   $rFileName = $record_data[video_name]."_".$record_data[video_cam_name]."_".$_POST["id"].".".$record_data[video_extension];
   //$fileName = $record_data[video_directory] . $rFileName;
   //$fileName = $record_data[video_directory] . $rFileName;
   $rawFilePath = $record_data[temp_vid_dir] . $rFileName;

   // Verifying extension to use certain transcoding codec
   if($record_data[video_extension] == "mp4"){
      $transcoding = ":sout='#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100,width=".$record_data[video_width].",height=".$record_data[video_height]."}";
      //echo "The selected recording extension is mp4!";
   } else if ($record_data[video_extension] == "webm"){
      $transcoding = ":sout='#transcode{vcodec=VP80,vb=2000,acodec=vorb,ab=128,channels=2,samplerate=44100,width=".$record_data[video_width].",height=".$record_data[video_height]."}";
      //echo "The selected recording extension is webm!";
   } else if ($record_data[video_extension] == "ogg"){
      $transcoding = ":sout='#transcode{vcodec=theo,vb=800,acodec=vorb,ab=128,channels=2,samplerate=44100,width=".$record_data[video_width].",height=".$record_data[video_height]."}";
      //echo "The selected recording extension is ogg!";
   }

   // Getting the command of record
   $duration = $record_data[max_saving_time];
   //$cmd = "cvlc " . $record_data[server_name] . ":" . $record_data[server_port] ."/" . $record_data[stream_file] . " " . $transcoding . ":std{access=file{overwrite},mux=".$record_data[video_extension].",dst='" . $rawFilePath . ".". $record_data[video_extension] ."'}'";
   $cmd = "cvlc --run-time=$duration http://localhost:" . $record_data[server_port] ."/" . $record_data[stream_file] . " " . $transcoding . ":std{access=file{overwrite},mux=".$record_data[video_extension].",dst='" . $rawFilePath . "'}' vlc://quit";

   // Executing the command
   exec($cmd . " > /dev/null 2>&1 & echo $!", $out, $return);

   if(!$return){
      //echo "this was a success. the code: \n";
      //echo $cmd;
      // If the log flag is true, get the log of the start of the recording
      if($record_data[log] == "true"){
      	$r = array("erro" => 1,
    	"msg" => "Unable to write to log...");
         $logFile = fopen("../logs/streaming_".date("Y").date("m").date("d").".log", "a") or die(json_encode($r));
         $debug_export = var_export($out, true);
         fwrite($logFile, date("H:i:s")." Started the recording of stream $rawFilePath.\n");
         fclose($logFile);
      }

      // Get the PID, the name of the file and the extension of the recording, to use to stop
      $pid = (int)$out[0];
      $rnameof = $rFileName;
      $extof = $record_data[video_extension];

      $r = array("erro" => 0,
      "msg" => "Gravação do vídeo iniciada.", "pid" => $pid, "rNameOf" => $rnameof, "extOf" => $extof);
      echo json_encode($r);

   } else {
      if($record_data[log] == "true"){
      	$r = array("erro" => 1,
    	"msg" => "Unable to write to log...");
         $logFile = fopen("../logs/streaming_".date("Y").date("m").date("d").".log", "a") or die(json_encode($r));
         $debug_export = var_export($out, true);
         fwrite($logFile, date("H:i:s")." Failed the recording of stream $rawFilePath. Reason:\n". $debug_export ."\n");
         fclose($logFile);
      }

      $r = array("erro" => 1,
      "msg" => "A gravação do vídeo falhou.");
      echo json_encode($r);
   }
