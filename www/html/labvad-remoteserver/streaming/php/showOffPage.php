<html>
<head>
   <title>LabVAD Server Video Module: Example Page</title>
   <script>
	function loadXMLGravacao()
	{
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","ajax_gravar.txt",true);
	xmlhttp.send();
	}

        function loadXMLParada()
	{
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","ajax_parar.txt",true);
	xmlhttp.send();
	}
   </script>
</head>
<body>
   <p>Abaixo, o player devera mostrar a stream</p>
   <?php
	$player_data = parse_ini_file("../streaming.conf");
	echo "<video width=\"".$player_data[player_width]."\" height=\"".$player_data[player_height]."\" autoplay>";
    echo "<source src=\"http://localhost:".$player_data[server_port]."/".$player_data[stream_file]."\" type=\"video/ogg\">";
    echo "</video>";
   ?>
   <br/>
   <div id="myDiv"><p>Atualmente, nenhum arquivo esta sendo gravado.<p></div>
   <form action="recordStream.php" target="_blank" method="post">
      <button type="submit" onclick="loadXMLGravacao()">Comecar gravacao</button>
   </form>
   <form action="stopStreamRecording.php" target="_blank" method="post">
      <button type="submit" onclick="loadXMLParada()">Parar gravacao</button>
   </form>
   <br/>
   <br/>
   <a href="videoList.php" target="_blank">Click here to see saved videos</a>
</body>
</html>
