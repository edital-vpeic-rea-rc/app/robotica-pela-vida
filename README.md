Labizinho: Robótica pela vida. Um projeto criado a partir do projeto LabVad (UFRJ).

A preciosa ajuda dos professores Fábio Ferrentini e Mauricio Nunes da Costa Bonfim, do Núcleo de Computação Eletrônica da UFRJ, e Leonardo Cunha, da UFRN, foi fundamental.

O projeto em cumprimento ao edital da FioCruz para atender as crianças do Instituto Fernandes Figueira.

Técnico responsável pelo aplicativo: Leonardo Martins
